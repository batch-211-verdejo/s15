console.log("Hello World");

let firstName = "John";
let lastName = "Smith";
let age = 40;
let hobbies = ["Biking", " Mountain Climbing", " swimming"];
let workAddress = {
    houseNumber: 32,
    street: "Washington",
    city: 'Lincoln',
    state: "Nebraska",
}

let friends = ["Tony ", "Bruce ", "Thor ", "Natasha ", "Clint ", "Nick"]
let fullProfile = {    
        userName: "captain_america",
        fullName: "Steve Rogers",
        age: 40,
        isActive: false,
    }



console.log("First Name: " + firstName);
console.log("Last Name:" + lastName);
console.log("Age: " + age)
console.log("hobbies");
console.log(hobbies);
console.log("Work Address");
console.log(workAddress);
console.log("My full name is: " + firstName +" "+ lastName)
console.log("My current age is: " + age);
console.log("My Friends are:")
console.log(friends);
console.log("My Full Profile:");
console.log(fullProfile);
console.log("My bestfriend is: Bucky Barnes");
console.log("I was found frozen in: Arctic Ocean");


